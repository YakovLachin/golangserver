package main

import (
	"net/http"
	"os"
	"time"
)

func main() {
	mux := http.NewServeMux()
	s := &http.Server{
		Addr:         "0.0.0.0:8080",
		Handler:      mux,
		ReadTimeout:  time.Duration(5 * time.Second),
		WriteTimeout: time.Duration(5 * time.Second),
	}
	mux.HandleFunc("/", Helloworld)

	sigchan := make(chan os.Signal)
	go s.ListenAndServe()
	<-sigchan

}

func Helloworld(writer http.ResponseWriter, req *http.Request) {
	writer.Write([]byte("hello from srv\n"))
}
