CWD=/go/src/gitlab.com/YakovLachin/golangserver
IMAGE=gitlab.com/YakovLachin/golangserver
test:
	@docker run --rm -v $(CURDIR):/app -w /app golang:1.8-alpine go test ./...

format:
	@docker run --rm -v $(CURDIR):/app -w /app golang:1.8-alpine gofmt -w /app

build:
	@docker build -t IMAGE .

up: create-network


create-network:
	@-docker network create app-net

