FROM golang:1.8.3-alpine as builder
WORKDIR /go/src/gitlab.com/YakovLachin/golangserver
COPY . .
RUN GOOS=linux GOARCH=amd64 go build -v -o rel/golangserver

FROM alpine:3.6
WORKDIR /golangserver
EXPOSE 8080
COPY --from=builder /go/src/gitlab.com/YakovLachin/golangserver/rel/golangserver ./
CMD ["./golangserver"]
